package com.pruebas.android.choreosapp;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.pruebas.android.choreosapp.models.Crime;

import java.util.HashMap;
import java.util.Map;

public class NewCrimeActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "NewCrimeActivity";
    private GoogleMap mMap;
    private DatabaseReference mDatabase;
    private FirebaseAuth auth;
    private FirebaseUser user;
    private boolean selectedLocation = false;

    private double lat;
    private double lng;
    private Crime crime;
    private EditText mTitleText;
    private EditText mDescriptionText;
    private AppCompatButton mCreateButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_crime);
        getSupportActionBar().setTitle(getResources().getString(R.string.new_crime));

        mDatabase = FirebaseDatabase.getInstance().getReference();
        user = FirebaseAuth.getInstance().getCurrentUser();

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        mTitleText = (EditText) findViewById(R.id.input_title);
        mDescriptionText = (EditText) findViewById(R.id.input_description);

        mCreateButton = (AppCompatButton) findViewById(R.id.btn_add);
        mCreateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (validForm()) {
                    submitCrime();
                    startActivity(new Intent(NewCrimeActivity.this, MainActivity.class));
                    finish();
                } else {
                    Toast.makeText(NewCrimeActivity.this,
                            getResources().getString(R.string.new_crime_form_error),
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    @Override
    public void onMapReady(final GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.setOnMapClickListener(new GoogleMap.OnMapClickListener() {
            @Override
            public void onMapClick(LatLng point) {
                mMap.clear();
                mMap.addMarker(new MarkerOptions().position(point));
                selectedLocation = true;
                lat = point.latitude;
                lng = point.longitude;
            }
        });

        String permission = "android.permission.ACCESS_FINE_LOCATION";
        int res = getApplicationContext().checkCallingOrSelfPermission(permission);

        if (res == PackageManager.PERMISSION_GRANTED) {
            mMap.setMyLocationEnabled(true);
        }

        LatLng currenPosition = new LatLng(-36.6057472, -72.098007);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(currenPosition, 13));
    }

    private void submitCrime() {
        String key = mDatabase.child("Crime").push().getKey();
        crime = new Crime(mTitleText.getText().toString(),
                user.getUid(), lat, lng, mDescriptionText.getText().toString());

        Map<String, Object> crimeValues = crime.toMap();
        Map<String, Object> childUpdates = new HashMap<>();

        childUpdates.put("/crimes/" + key, crimeValues);

        mDatabase.updateChildren(childUpdates);
    }

    private boolean validForm() {
        if (TextUtils.isEmpty(mTitleText.getText().toString())) {
            mTitleText.setError(getResources().getString(R.string.title_error));
            mTitleText.requestFocus();
            return false;
        }

        if (TextUtils.isEmpty(mDescriptionText.getText().toString())) {
            mDescriptionText.setError(getResources().getString(R.string.description_error));
            mDescriptionText.requestFocus();
            return false;
        }

        if (!selectedLocation) {
            Toast.makeText(this, getResources().getString(R.string.marker_error), Toast.LENGTH_LONG).show();
            return false;
        }

        return true;
    }
}
