package com.pruebas.android.choreosapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;
import com.pruebas.android.choreosapp.models.Crime;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements OnMapReadyCallback {
    private static final String TAG = "MainActivity";
    private GoogleMap mMap;
    private FloatingActionButton fabAdd;
    private FirebaseAuth auth;
    private DatabaseReference mCrimesReference;
    private ArrayList<Marker> markers;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        fabAdd = (FloatingActionButton) findViewById(R.id.fab);
        fabAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(MainActivity.this, NewCrimeActivity.class));
            }
        });

        auth = FirebaseAuth.getInstance();
        mCrimesReference = FirebaseDatabase.getInstance().getReference().child("crimes");
        markers = new ArrayList<>();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        LatLng center = new LatLng(-36.6057472, -72.098007);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(center, 13));
        //CameraPosition cameraPosition = new CameraPosition.Builder().target(center).zoom(14).tilt(60).build();
        //mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));

        mCrimesReference.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                Log.d(TAG, "Se agrego un nuevo crimen.");
                double lat = (double) dataSnapshot.child("lat").getValue();
                double lng = (double) dataSnapshot.child("lng").getValue();
                String description = (String) dataSnapshot.child("description").getValue();
                String title = (String) dataSnapshot.child("title").getValue();

                LatLng marker = new LatLng(lat, lng);
                Marker newMarker = mMap.addMarker(new MarkerOptions().position(marker).title(title).snippet(description));
                markers.add(newMarker);
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(marker, 13));
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
                double lat = (double) dataSnapshot.child("lat").getValue();
                double lng = (double) dataSnapshot.child("lng").getValue();
                String title = (String) dataSnapshot.child("title").getValue();
                String description = (String) dataSnapshot.child("description").getValue();

                for (Marker marker : markers) {
                    if (marker.getPosition().latitude == lat &&
                            marker.getPosition().longitude == lng) {
                        marker.setTitle(title);
                        marker.setSnippet(description);
                        marker.hideInfoWindow();
                        marker.showInfoWindow();
                    }
                }
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
                double lat = (double) dataSnapshot.child("lat").getValue();
                double lng = (double) dataSnapshot.child("lng").getValue();

                for (Marker marker : markers) {
                    if (marker.getPosition().latitude == lat &&
                            marker.getPosition().longitude == lng) {
                        marker.remove();
                    }
                }
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String s) {

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_logout) {
            auth.signOut();
            startActivity(new Intent(this, StartActivity.class));
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

}
