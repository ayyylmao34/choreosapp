package com.pruebas.android.choreosapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;

public class LoginFragment extends Fragment {
    private ProgressDialog progress;
    private AppCompatButton mLoginButton;
    private EditText mEmailText;
    private EditText mPasswordText;
    private TextView mResetPasswordText;
    private FirebaseAuth auth;

    public LoginFragment() { }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        auth = FirebaseAuth.getInstance();

        if (auth.getCurrentUser() != null) {
            startActivity(new Intent(getActivity(), MainActivity.class));
            getActivity().finish();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_login, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mEmailText = (EditText) view.findViewById(R.id.input_email);
        mPasswordText = (EditText) view.findViewById(R.id.input_password);

        mLoginButton = (AppCompatButton) view.findViewById(R.id.btn_login);
        mLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validate()) {
                    Toast.makeText(getContext(),
                            getResources().getString(R.string.login_failed), Toast.LENGTH_LONG);
                } else {

                    progress = new ProgressDialog(getActivity());
                    progress.setMessage(getResources().getString(R.string.logging_in));
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setProgress(0);
                    progress.show();

                    String email = mEmailText.getText().toString();
                    String password = mPasswordText.getText().toString();
                    auth.signInWithEmailAndPassword(email, password)
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progress.setProgress(100);
                                progress.hide();

                                if (!task.isSuccessful()) {
                                    Toast.makeText(getContext(),
                                            getResources().getString(R.string.login_failed),
                                            Toast.LENGTH_LONG).show();
                                    mEmailText.setError(getResources().getString(R.string.email_error));
                                } else {
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                    getActivity().finish();
                                }
                            }
                        });
                }
            }
        });

        mResetPasswordText = (TextView) view.findViewById(R.id.reset_password);
        mResetPasswordText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(getActivity(), ResetPassword.class));
                getActivity().finish();
            }
        });
    }

    private boolean validate() {
        boolean valid = true;

        String email = mEmailText.getText().toString();
        String password = mPasswordText.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailText.setError(getResources().getString(R.string.email_error));
            valid = false;
        } else {
            mEmailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            mPasswordText.setError(getResources().getString(R.string.password_error));
            valid = false;
        } else {
            mPasswordText.setError(null);
        }

        return valid;
    }
}
