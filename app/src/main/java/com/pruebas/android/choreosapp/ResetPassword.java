package com.pruebas.android.choreosapp;

import android.app.ProgressDialog;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatButton;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

public class ResetPassword extends AppCompatActivity {
    private ProgressDialog progress;
    private FirebaseAuth auth;
    private EditText mEmailText;
    private AppCompatButton mResetPasswordButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        auth = FirebaseAuth.getInstance();

        mEmailText = (EditText) findViewById(R.id.input_email);
        mResetPasswordButton = (AppCompatButton) findViewById(R.id.btn_reset_password);
        mResetPasswordButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!validate()) {
                    showToast(getResources().getString(R.string.email_error));
                    return;
                }

                progress = new ProgressDialog(view.getContext());
                progress.setMessage(getResources().getString(R.string.account_reseting));
                progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                progress.setIndeterminate(true);
                progress.setProgress(0);
                progress.show();

                auth.sendPasswordResetEmail(mEmailText.getText().toString())
                        .addOnCompleteListener(new OnCompleteListener<Void>() {

                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            showToast(getResources().getString(R.string.account_reset));
                            mEmailText.setText("");
                            startActivity(new Intent(ResetPassword.this, StartActivity.class));
                        } else {
                            showToast(getResources().getString(R.string.error_account_reset));
                        }

                        progress.setProgress(100);
                        progress.hide();
                    }
                });
            }
        });
    }

    private boolean validate() {
        boolean valid = true;
        String email = mEmailText.getText().toString();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailText.setError(getResources().getString(R.string.email_error));
            valid = false;
        } else {
            mEmailText.setError(null);
        }

        return valid;
    }

    private void showToast(String text) {
        Toast.makeText(ResetPassword.this, text, Toast.LENGTH_LONG).show();
    }
}
