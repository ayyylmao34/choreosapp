package com.pruebas.android.choreosapp.models;

import com.google.firebase.database.Exclude;
import com.google.firebase.database.IgnoreExtraProperties;

import java.util.HashMap;
import java.util.Map;

@IgnoreExtraProperties
public class Crime {
    public String title;
    public String uid;
    public double lat;
    public double lng;
    public String description;

    public Crime() {}

    public Crime(String title, String uid, double lat, double lng, String description) {
        this.title = title;
        this.uid = uid;
        this.lat = lat;
        this.lng = lng;
        this.description = description;
    }

    @Exclude
    public Map<String, Object> toMap() {
        HashMap<String, Object> result = new HashMap<>();

        result.put("title", title);
        result.put("uid", uid);
        result.put("lat", lat);
        result.put("lng", lng);
        result.put("description", description);

        return result;
    }
}
