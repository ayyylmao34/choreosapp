package com.pruebas.android.choreosapp;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.AppCompatButton;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;


public class SignUpFragment extends Fragment {
    private ProgressDialog progress;
    private AppCompatButton mSignupButton;
    private EditText mEmailText;
    private EditText mPasswordText;
    private FirebaseAuth auth;

    public SignUpFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sign_up, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        auth = FirebaseAuth.getInstance();
        mEmailText = (EditText) view.findViewById(R.id.input_email_signup);
        mPasswordText = (EditText) view.findViewById(R.id.input_password_signup);

        mSignupButton = (AppCompatButton) view.findViewById(R.id.btn_signup);
        mSignupButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!validate()) {
                    showMessage(getResources().getString(R.string.signup_failed));
                } else {
                    progress = new ProgressDialog(getActivity());
                    progress.setMessage(getResources().getString(R.string.checking_in));
                    progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                    progress.setIndeterminate(true);
                    progress.setProgress(0);
                    progress.show();

                    auth.createUserWithEmailAndPassword(getEmail(), getPassword())
                        .addOnCompleteListener(getActivity(), new OnCompleteListener<AuthResult>() {
                            @Override
                            public void onComplete(@NonNull Task<AuthResult> task) {
                                progress.setProgress(100);

                                if (!task.isSuccessful()) {
                                    showMessage(getResources().getString(R.string.signup_failed));
                                } else {
                                    startActivity(new Intent(getActivity(), MainActivity.class));
                                    getActivity().finish();
                                }
                            }
                        }).addOnFailureListener(getActivity(), new OnFailureListener() {
                            @Override
                            public void onFailure(@NonNull Exception e) {
                                progress.setProgress(100);
                                showMessage(getResources().getString(R.string.signup_failed));
                            }
                        });
                }
            }
        });
    }

    public String getEmail() {
        return mEmailText.getText().toString();
    }

    public String getPassword() {
        return mPasswordText.getText().toString();
    }

    public boolean validate() {
        boolean valid = true;

        String email = getEmail();
        String password = getPassword();

        if (email.isEmpty() || !Patterns.EMAIL_ADDRESS.matcher(email).matches()) {
            mEmailText.setError(getResources().getString(R.string.email_error));
            valid = false;
        } else {
            mEmailText.setError(null);
        }

        if (password.isEmpty() || password.length() < 4 || password.length() > 10) {
            mPasswordText.setError(getResources().getString(R.string.password_error));
            valid = false;
        } else {
            mPasswordText.setError(null);
        }

        return valid;
    }

    public void showMessage(String text) {
        Toast.makeText(getActivity(), text, Toast.LENGTH_LONG).show();
    }
}
